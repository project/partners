<?php


/////// @@TODO: adding 'tree' => TRUE to form elements will prevent collapse of hierarchy when passed to $form_state
//!!!!!






/**
 * Alter the field settings form for reference fields.
 */
function partners_content_field_edit_form_alter(&$form, $form_state, $form_id) {

	if($form['#field']['type'] == 'nodereference') {

		$content_type = $form['#field']['type_name'];
		$field_name = $form['#field']['field_name'];
		$type = $form['type_name']['#value'];

		$bidirectionality_options = array();
		$bidirectionality_settings = variable_get('partners_bidirectionality', array());
		$types = content_types();

		// Build the list of possible bidirectionality pair fields (has to be a noderef field able to point at this content type)
		foreach($types as $type_id => $type_array) {
			foreach($type_array['fields'] as $field) {
				if($field['type'] == 'nodereference') {
					if($form['#field']['referenceable_types'][$type_id] === $type_id
						&& $field['referenceable_types'][$content_type] === $content_type) {	// If the noderef field is allowed to ref the content type we're editing
						$bidirectionality_options[$type_id . '.' . $field['field_name']] = '('.$type_id.') ' . $field['field_name'];	// Values are 'NODE_TYPE.FIELD_NAME'
					}
				}
			}
		}

		// Set defaults for the bidirectionality section
		$bidirectionality_defaults = array();
		foreach($bidirectionality_options as $k => $v) {
			$field_uri = explode('.', $k);
			if(count($field_uri) > 1) { // Error checking
				list($foreign_type, $foreign_field) = $field_uri;
				if($bidirectionality_settings[$content_type][$field_name][$foreign_type][$foreign_field] == '1') // If this is set in the db-saved settings
					$bidirectionality_defaults[$k] = $k; // ... then use it as the default value for the form field
			}

		}
		
		// Bidirectionality noderef field pairing form thing
		$form['widget']['partners'] = array(
			'#type' => 'fieldset',
			'#title' => t('Partners settings'),
		);

		$form['widget']['partners']['fields_to_pair'] = array(
			'#type' => 'checkboxes',
			'#title' => 'Bidirectionality - fields with which to pair this nodereference field',
			'#description' => t('Fields will only show up here if they are able to point at ' . $type . ' nodes.'),
			'#options' => $bidirectionality_options,
			'#default_value' => $bidirectionality_defaults,	// @@TODO: default values
		);

		// Intermediary options
		$intermediary_options = node_get_types('types');
		array_walk($intermediary_options, create_function('&$v, $k', '$v = $k;'));
		$intermediary_options = array('--' => 'None') + $intermediary_options;
		$intermediary_settings = variable_get('partners_intermediaries',array());

		foreach($form['#field']['referenceable_types'] as $foreign_type => $allowed) {
			// if foreign type can be referenced
			if($allowed != '0') {
				$a_bidirectional_relationship_exists = FALSE;
			
				// discern whether foreign field is bidirectional
				foreach((array)$bidirectionality_settings[$content_type][$field_name][$foreign_type] as $foreign_field => $enabled) {
					if($enabled == '1') {
						$a_bidirectional_relationship_exists = TRUE;
						break;
					}
				}
 
				if($a_bidirectional_relationship_exists) {	// If there's bidirectionality with this foreign_type, allow intermediaries for each paired field
					foreach((array)$bidirectionality_settings[$content_type][$field_name][$foreign_type] as $foreign_field => $enabled) {
						if($enabled == '1') {
							$form['widget']['partners']['intermediaries']['intermediary:'.$foreign_type.':'.$foreign_field] = array(
								'#type' => 'select',
								'#title' => 'Intermediary node for bidirectional relationships with ' . $foreign_type . '.<em>' . $foreign_field . '</em>',
								'#options' => $intermediary_options,
								'#default_value' => ($intermediary_settings[$content_type][$field_name][$foreign_type][$foreign_field] ? $intermediary_settings[$content_type][$field_name][$foreign_type][$foreign_field] : '--'),
							);
						}
					}
				}
				else {	// Otherwise, just provide intermediary options for the foreign_type as a whole
					$form['widget']['partners']['intermediaries']['intermediary:'.$foreign_type] = array(
						'#type' => 'select',
						'#title' => 'Intermediary node for one-way relationships with ' . $foreign_type . ' nodes',
						'#options' => $intermediary_options,
						'#default_value' => ($intermediary_settings[$content_type][$field_name][$foreign_type] ? $intermediary_settings[$content_type][$field_name][$foreign_type] : '--'),
					);
				}

			}
		}
		
		$form['#submit'][] = 'partners_content_field_edit_form_submit';
	}
}

/**
 * Submit handler for nodereference field settings form.
 */
function partners_content_field_edit_form_submit($form, &$form_state) {

	$content_type = $form['#field']['type_name'];
	$field_name = $form['#field']['field_name'];
	$form_values = $form_state['values'];

/* $form_values['fields_to_pair'] looks like:
		Array
		(
				[FOREIGN_NODE_TYPE.FOREIGN_FIELD] => 0 if not checked, FOREIGN_NODE_TYPE.FOREIGN_FIELD if checked
				[link.field_linked_things] => link.field_linked_things
				[thing.field_linked_things] => thing.field_linked_things
				[link.field_thing_refs] => 0		// this is an unset checkbox
		)
*/

/*
		variable_get('partners_bidirectionality') looks like:
		Array
		(	 [thing] => Array (
					[field_linked_things] => Array (
						[link] => Array (
							[field_linked_things] => 1
							[field_thing_refs] => 1
						)
						[thing] => Array(
							[field_linked_things] => 1
						)
					)
				)
			[link] => Array(
					[field_linked_things] => Array(
						[thing] => Array(
							[field_linked_things] => 1
						)
					)
					[field_thing_refs] => Array(
						[thing] => Array(
							[field_linked_things] => 1
						)
					)
				)
		)
*/

	// Filter out bidirectionality settings that don't match up with the global 'referenceable nodes' settings
	foreach($form_values['referenceable_types'] as $type => $type_enabled) {
		if($type_enabled == '0') {
			foreach($form_values['fields_to_pair'] as $paired => $e) { // This takes care of 'NODE_TYPE' as well as 'NODE_TYPE.FIELD_NAME'
				list($paired_type, $paired_field) = explode('.', $paired);
				if($paired_type == $type) unset($form_values['fields_to_pair'][$paired]);
			}
		}
	}

	// Do bidirectionality
	$settings = variable_get('partners_bidirectionality', array());
	$settings[$content_type][$field_name] = array();
	
	foreach($form_values['fields_to_pair'] as $k => $v) {
		list($foreign_type, $foreign_field) = explode('.', $k);
		if($v == '0')																															 // If this bidirectional relationship is not activated/checked
			$settings[$content_type][$field_name][$foreign_type][$foreign_field] = 0; // ... then unset it
		else																																				// If it IS activated/checked
			$settings[$content_type][$field_name][$foreign_type][$foreign_field] = 1; // ... then set it
	}

	// Set opposite field's bidirectionality settings too (for denormalization purposes)
	foreach($form_values['fields_to_pair'] as $k => $v) {
		list($foreign_type, $foreign_field) = explode('.', $k);
		if($v == '0')																															 // If this bidirectional relationship is not activated/checked
			$settings[$content_type][$field_name][$foreign_type][$foreign_field] = 0; // ... then unset it
		else																																				// If it IS activated/checked
			$settings[$foreign_type][$foreign_field][$content_type][$field_name] = 1; // ... then set it
	}
	variable_set('partners_bidirectionality', $settings);

	// Do intermediary settings
	$intermediaries = array();

	foreach($form_values as $k => $v) {
		$field = explode(':', $k);
		
		// The field is "intermediary:NODE_TYPE", indicating a field without a bidirectional link to another field
		if(count($field) == 2 and $field[0] == 'intermediary') {
			$foreign_type = $field[1];
			if($form['#field']['referenceable_types'][$foreign_type] == '0') continue;
			if($v == '--') continue; // If set to 'none' don't save to db so array stays sparse
		
			$intermediaries[$content_type][$field_name][$foreign_type] = $v;
		}
		// The field is "intermediary:NODE_TYPE:field_name", indicating a field with a bidirectional link to another field
		else if(count($field) == 3 and $field[0] == 'intermediary') {
			$foreign_type = $field[1];
			$foreign_field = $field[2];
			if($form['#field']['referenceable_types'][$foreign_type] == '0') continue;
			if($v == '--') continue; // If set to 'none' don't save to db so array stays sparse
		
			$intermediaries[$content_type][$field_name][$foreign_type][$foreign_field] = $v;
		}
	}

	variable_set('partners_intermediaries', $intermediaries);
}



